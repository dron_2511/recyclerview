package com.example.android.recyclerview.alarmmanager;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.recyclerview.R;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Android on 08.12.2015.
 */
public class AlarmManagerActivity extends Activity {
    private PendingIntent pendingIntent;
    private AlarmManager manager;
    private Intent intent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        //Вывод названии всех датчиков на устройстве
        SensorManager manager1 = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> sensors = manager1.getSensorList(Sensor.TYPE_ALL);
        TextView text = (TextView) findViewById(R.id.e);
        text.append("Available sensors:");
        for (Sensor s : sensors) {
            text.append("\n" + s.getName());
        }


        manager = (AlarmManager) getSystemService(ALARM_SERVICE);
        intent = new Intent(AlarmManagerActivity.this, AlarmService.class);
        pendingIntent = PendingIntent.getService(getApplicationContext(), 0, intent, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.SECOND, 3);
        manager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 30000, pendingIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(AlarmManagerActivity.this, AlarmService.class));
        Toast.makeText(this, "Destroy", Toast.LENGTH_SHORT).show();
    }
}
