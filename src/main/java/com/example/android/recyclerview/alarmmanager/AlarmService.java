package com.example.android.recyclerview.alarmmanager;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.os.BatteryManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.example.android.recyclerview.R;

/**
 * Created by Android on 08.12.2015.
 */
public class AlarmService extends Service {
    private BroadcastReceiver receiver;
    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(AlarmService.this, "Service create", Toast.LENGTH_SHORT).show();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Вывод данных о Battery : BATTERY_STATUS, BATTERY_PLUGGED, и уровень заряда
        battery();
        registerReceiver(receiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopSelf();
        Toast.makeText(AlarmService.this, "Stop service", Toast.LENGTH_SHORT).show();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void battery() {
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int status = intent.getIntExtra(
                        BatteryManager.EXTRA_STATUS, -1);
                int plugged = intent.getIntExtra(
                        BatteryManager.EXTRA_PLUGGED, -1);
                int level = intent.getIntExtra(
                        BatteryManager.EXTRA_LEVEL, -1);
                int scale = intent.getIntExtra(
                        BatteryManager.EXTRA_SCALE, -1);

                // Состояние зарядки батареи
                String sStatus = "Not reported";
                switch (status) {
                    case BatteryManager.BATTERY_STATUS_CHARGING:
                        Toast.makeText(AlarmService.this, "BATTERY_STATUS_CHARGING ", Toast.LENGTH_SHORT).show();
                        break;
                    case BatteryManager.BATTERY_STATUS_DISCHARGING:
                        Toast.makeText(AlarmService.this, "BATTERY_STATUS_DISCHARGING ", Toast.LENGTH_SHORT).show();
                        break;
                    case BatteryManager.BATTERY_STATUS_FULL:
                        Toast.makeText(AlarmService.this, " BATTERY_STATUS_FULL", Toast.LENGTH_SHORT).show();
                        break;
                    case BatteryManager.BATTERY_STATUS_NOT_CHARGING:
                        Toast.makeText(AlarmService.this, " BATTERY_STATUS_NOT_CHARGING", Toast.LENGTH_SHORT).show();
                        break;
                    case BatteryManager.BATTERY_STATUS_UNKNOWN:
                        Toast.makeText(AlarmService.this, "BATTERY_STATUS_UNKNOWN", Toast.LENGTH_SHORT).show();
                        break;
                }

                // Тип зарядки
                String splugged = "Not Reported";
                switch (plugged) {
                    case BatteryManager.BATTERY_PLUGGED_AC:
                        Toast.makeText(AlarmService.this, "BATTERY_PLUGGED_AC", Toast.LENGTH_SHORT).show();
                        break;
                    case BatteryManager.BATTERY_PLUGGED_USB:
                        Toast.makeText(AlarmService.this, "BATTERY_PLUGGED_USB", Toast.LENGTH_SHORT).show();
                        break;
                }
                int chargedPct = (level * 100) / scale;
                Toast.makeText(AlarmService.this, chargedPct + "%", Toast.LENGTH_SHORT).show();
            }
        };
    }
}
