package com.example.android.recyclerview;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.ClipData;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Android on 07.12.2015.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private List<Item> dataset;
    public String TAG = "Service";

    public RecyclerViewAdapter(List<Item> dataset) {
        this.dataset = dataset;
    }


    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row, parent, false);
        Log.i(TAG, "onCreateViewHolder");
        //ObjectAnimator animator = ObjectAnimator.ofFloat(view, View.ROTATION_X, 0f, 360f).setDuration(2000);
//        animator.setRepeatCount(3);

        ObjectAnimator animator1 = ObjectAnimator.ofFloat(view, View.ROTATION_Y, 0f, 360f).setDuration(2000);
//        animator1.setRepeatCount(3);

        ObjectAnimator animator2 = ObjectAnimator.ofFloat(view, View.ROTATION, 0f, 360f).setDuration(1000);
//        animator2.setRepeatCount(3);
        // ObjectAnimator animator3 = ObjectAnimator.ofFloat(view, View.SCALE_X, 0.5f, 1f).setDuration(4000);
//        animator3.setRepeatCount(3);
        //ObjectAnimator animator4 = ObjectAnimator.ofObject(view, View.Y, null, 100f, 1000f).setDuration(5000);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animator1, animator2);
        animatorSet.start();

        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapter.ViewHolder holder, int position) {

        holder.getAge().setText(dataset.get(position).getAge());
        holder.getName().setText(dataset.get(position).getName());
        Log.i(TAG, "onBindViewHolder");
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private TextView age;


        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.txtName);
            age = (TextView) itemView.findViewById(R.id.txtAge);
        }

        public TextView getName() {
            return name;
        }

        public TextView getAge() {
            return age;
        }
    }
}
