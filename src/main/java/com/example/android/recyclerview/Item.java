package com.example.android.recyclerview;

import android.widget.ProgressBar;

/**
 * Created by Android on 07.12.2015.
 */
public class Item {
    private String name;
    private String age;
    private ProgressBar bar;

    public ProgressBar getBar() {
        return bar;
    }

    public void setBar(ProgressBar bar) {
        this.bar = bar;
    }

    public String getName() {
        return name;
    }

    public Item(String name, String age) {
        this.name = name;
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}
