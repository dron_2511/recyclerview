package com.example.android.recyclerview;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.android.recyclerview.alarmmanager.AlarmManagerActivity;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Item> arrayList;
    private RecyclerView recycler;
    private int count = 1;
    private RecyclerViewAdapter recyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//      Intent intent = new Intent(this, AlarmManagerActivity.class);
//      startActivity(intent);

        arrayList = new ArrayList<>();
        arrayList.add(new Item("1", "1"));
        recyclerViewAdapter = new RecyclerViewAdapter(arrayList);
        recycler = (RecyclerView) findViewById(R.id.listView);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setAdapter(recyclerViewAdapter);
    }

    public void onClickItemAdd(View view) {
//        count++;
//        arrayList.add(new Item("" + count, "" + count));
//       // recycler.setAdapter(new RecyclerViewAdapter(arrayList));
//        recycler.swapAdapter(new RecyclerViewAdapter(arrayList), false);
        count++;
        if (recycler.getAdapter() != null) {
            arrayList.add(new Item("" + count, "" + count));
            recycler.swapAdapter(recyclerViewAdapter, false);
        } else {
            recycler.setAdapter(recyclerViewAdapter);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, AlarmManagerActivity.class));
        Toast.makeText(MainActivity.this, "Main Activity Service destroy", Toast.LENGTH_LONG).show();
    }
}
